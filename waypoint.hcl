project = "semantic-release"

app "semantic-release" {
  build {
    use "docker" {
      disable_entrypoint = true
      dockerfile = "Containerfile"

      auth {
        username = var.dependency_registry_username
        password = var.dependency_registry_password
      }
    }

    registry {
      use "docker" {
        image = var.registry_image_name
        tag   = var.registry_image_tag

        username = var.registry_username
        password = var.registry_password
      }
    }
  }

  deploy {
    use "nomad" {}
  }
}

variable "registry_username" {
  type        = string
  default     = "username"
  env         = ["CI_REGISTRY_USER"]
  description = "The username to log in to the docker registry"
  sensitive   = false
}

variable "registry_password" {
  type        = string
  default     = "username"
  env         = ["CI_REGISTRY_PASSWORD"]
  description = "The password to log in to the docker registry"
  sensitive   = true
}

variable "registry_image_name" {
  type        = string
  env         = ["CI_REGISTRY_IMAGE"]
  description = "The docker image name"
  sensitive   = false
}

variable "registry_image_tag" {
  type        = string
  env         = ["CI_COMMIT_TAG", "CI_COMMIT_SHA"]
  description = "The docker image tag"
  sensitive   = false
}

variable "dependency_registry_username" {
  type        = string
  description = "The username to log in to the docker registry"
  env         = ["CI_DEPENDENCY_PROXY_USER"]
  sensitive   = false
}

variable "dependency_registry_password" {
  type        = string
  description = "The password to log in to the docker registry"
  env         = ["CI_DEPENDENCY_PROXY_PASSWORD"]
  sensitive   = true
}
