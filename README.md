# semantic-release image

This project provides docker images containing [semantic-release](https://github.com/semantic-release/semantic-release/blob/master/docs/recipes/ci-configurations/gitlab-ci.md).

Information can be found in the docs/ folder or on https://rwthapp.pages.rwth-aachen.de/misc/container-images/semantic-release/
